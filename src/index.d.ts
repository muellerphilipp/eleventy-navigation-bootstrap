declare module 'eleventy-navigation-bootstrap' {
  export default function toBootstrapNav(pages: Array<navigationPage>, options: navigationOptions): string
}

interface navigationOptions {
  listElement?: string,
  listClass?: string,

  listItemElement?: string,
  listItemClass?: string,
  listItemLinkClass?: string,

  listItemHasChildrenClass?: string,
  listItemHasChildrenLinkClass?: string,

  listChildItemClass?: string,
  listChildItemLinkClass?: string,

  activeKey?: string,
  activeListItemClass?: string,
  listDoorpageDivider?: string,

  isChildList?: string,
  parentEntry?: navigationPage
}

interface navigationPage {
  children: Array<navigationPage>,
  dropdownLabel?: string,
  doorpageLabel?: string,
  key: string,
  order: number,
  parent: string,
  parentKey: string,
  pluginType: string,
  title: string,
  url: string
}