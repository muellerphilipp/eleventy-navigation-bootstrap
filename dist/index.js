const urlFilter = require('@11ty/eleventy/src/Filters/Url');
const toBootstrapNav = (pages, options = {}) => {
    function navItem(entry) {
        function activeClass() {
            const hasActiveChildren = options.activeKey === entry.key
                || !!entry.children.find((child) => child.key === options.activeKey);
            return hasActiveChildren ? options.activeListItemClass : '';
        }
        const linkActive = options.activeKey === entry.key || entry.doorpageLabel ?
            `<span class="${isChildList ? options.listChildItemLinkClass : options.listItemLinkClass} ${options.activeListItemClass}" tabindex="-1" aria-current="true">${entry.title}</span>`
            : `<a href="${urlFilter(entry.url)}" class="${isChildList ? options.listChildItemLinkClass : options.listItemLinkClass}" role="menuitem">${entry.title}</a>`, linkDropdown = `${toBootstrapNav(entry.children, Object.assign(options, { isChildList: entry.key, parentEntry: entry }))}`, linkHasChildren = entry.children && entry.children.length ?
            `<span id="sub${entry.key}" class="${options.listItemLinkClass} ${options.listItemHasChildrenLinkClass} ${activeClass()}" data-toggle="${options.listItemHasChildrenClass}" href="#" role="button" aria-haspopup="true" aria-expanded="false">${entry.title}</span>
             ${linkDropdown}`
            : linkActive;
        return `<${options.listItemElement} class="${options.listItemClass}${entry.children && entry.children.length ? ` ${options.listItemHasChildrenClass}` : ''}">
        ${linkHasChildren}
      </${options.listItemElement}>`;
    }
    function doorpageItem(entry) {
        const doorpageItemLink = options.activeKey === entry.key ?
            `<span class="${options.listChildItemLinkClass} ${options.activeListItemClass}" tabindex="-1" aria-current="true">${entry.doorpageLabel}</span>`
            : `<a href="${urlFilter(entry.url)}" class="${options.listChildItemLinkClass}" role="menuitem">${entry.doorpageLabel}</a>`;
        return `<${options.listItemElement} class="${options.listItemClass}">
        ${doorpageItemLink}
      </${options.listItemElement}>
      ${options.listDoorpageDivider}`;
    }
    if (pages.length && pages[0].pluginType !== 'eleventy-navigation') {
        throw new Error('Incorrect argument passed to toBootstrapNav filter. You must call `eleventyNavigation` or `eleventyNavigationBreadcrumb` first, like: `collection.all | eleventyNavigation | toBootstrapNav | safe`');
    }
    options = Object.assign({
        listElement: 'ul',
        listClass: 'nav',
        listItemElement: 'li',
        listItemClass: 'nav-item',
        listItemLinkClass: 'nav-link',
        listItemHasChildrenClass: 'dropdown',
        listItemHasChildrenLinkClass: 'dropdown-toggle',
        listChildItemClass: 'dropdown-menu',
        listChildItemLinkClass: 'dropdown-item',
        activeKey: '',
        activeListItemClass: 'active',
        listDoorpageDivider: '<div class="dropdown-divider" role="divider"></div>',
        isChildList: '',
        parentEntry: null
    }, options);
    const isChildList = options.isChildList, listClass = isChildList ?
        `class="${options.listChildItemClass}" aria-labelledby="sub${isChildList}"`
        : `class="${options.listClass}"`;
    return pages.length ?
        `<${options.listElement} ${listClass}>
      ${isChildList && options.parentEntry !== null ? doorpageItem(options.parentEntry) : ''}
      ${pages.map(navItem).join('\n')}
    </${options.listElement}>`
        : '';
};
module.exports = toBootstrapNav;
//# sourceMappingURL=index.js.map