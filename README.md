# eleventy-navigation-bootstrap

Filter for the @11ty/eleventy-navigation plugin to support the bootstrap dropdown navigation.

## Usage

Install via npm:

```bash
npm install --save-dev eleventy-navigation-bootstrap
```

Add this Nunjucks filter to your [.eleventy.js](https://www.11ty.dev/docs/config/) file:

```javascript
  const toBootstrapNav = require('eleventy-navigation-bootstrap')
  eleventyConfig.addNunjucksFilter('bootstrapNav', toBootstrapNav)
```


Pipe the [@11ty/eleventy-navigation](https://github.com/11ty/eleventy-navigation) output to this filter like so:

```nunjucks
{{ collections.all | eleventyNavigation | bootstrapNav | safe }}
```

## Options

You can pass additional options:

```nunjucks
{{ collections.all | eleventyNavigation | bootstrapNav({
    listClass: "nav nav-pills justify-content-center", listChildItemClass: "dropdown-menu shadow", activeKey: eleventyNavigation.key
  }) | safe }}
```

Available options are:

Option | Type | Effect
--- | --- | ---
`listElement` | string  | `HTML` tag of the navigation list
`listClass` | string  | Class for the navigation list
`listItemElement` | string  | `HTML` tag for one navigation entry
`listItemClass` | string  | Class for one navigation entry
`listItemLinkClass` | string  | Class for the link of a navigation entry
`listItemHasChildrenClass` | string | Class for a navigation entry that has child entries
`listItemHasChildrenLinkClass` | string | Class for the link of a navigation entry that has child entries
`listChildItemClass` | string | Class for a navigation entry that is child to another entry
`listChildItemLinkClass` | string | Class for the link of a navigation entry that is child to another entry
`activeKey` | string  | Current page (provide this if you want to highlight the currently displayed page)
`activeListItemClass` | string  | Class of the navigation entry that corresponds to the current page
`listDoorpageDivider` | string | Divider between the door page and the child entries of a parent page